# Google Calendar Sync

## Requirements

1. You need 2 google calendar account
1. You need to share your work calendar to your personnal account with write access.

## Installation

1. Go to [Google Script](https://script.google.com/home)
1. Add a new project, name it
1. Go to Settings > Tick _Display manifest "appscript.json"_
1. Copy content of 
    1. [Code.gs](./Code.gs) into `Code.gs`
    1. [appsscript.json](./appsscript.json) into `appsscript.json`
1. Modify 2 first constants to match your calendars
```gs
const WORK_CALENDAR = '<pro email>'
const PERSONNAL_CALENDAR = '<personnal email>'
```
1. Configure trigger on calendar change
    1. Go to Triggers
    2. Add a new trigger
        
        | | |
        |:---|:---|
        |_Choose which function to run_ | `main` |
        | _Choose which deployment should run_ | `Head` |
        | _Select event source_ | `From calendar` | 
        | _Enter calendar details_ | `Calendar updated` |
        | _Calendar owner email_ | 1 of your 2 email (work, personnal) |
    
    3. Add another trigger with the second email address    
