// Configuration

const WORK_CALENDAR = '<pro email>'
const PERSONNAL_CALENDAR = '<personnal email>'

// Script

const MAX_DAYS_TO_SYNC = 180
const EVENT_SUMMARY = 'Not available'
const MAX_REQUEST_PER_BATCH = 50

const SYNC = {
  SOURCE: 'source',
  TARGET: 'target'
}

const WAIT_BETWEEN_BATCH_CALL = 2000 // milliseconds
let LATEST_BATCH_TIME = 0

/**
 * Calendar syncer
 *  
 * This script uses advanced calendar API
 * @see https://developers.google.com/apps-script/advanced/calendar#listing_calendars
*/
class SyncCalendar {
  constructor(sourceCalendarId, targetCalendarId, fullSync = false) {
    this.fullSync = fullSync
    this.sourceCalendar = Calendar.Calendars.get(sourceCalendarId)
    this.targetCalendar = Calendar.Calendars.get(targetCalendarId)
    this.report = {
      source: this.sourceCalendar.summary,
      target: this.targetCalendar.summary,
      created: { total: 0, failed: 0 },
      patched: { total: 0, failed: 0 },
      deleted: { total: 0, failed: 0 }
    }
  }

  getDefaultOptions() {
    return {
      maxResults: 250,
      singleEvents: true
    }
  }

  /**
   * Returns option to query events based on time
   */
  getTimedEventOptions(options) {
    const queryOption = this.getDefaultOptions()

    const startTime = new Date()
    startTime.setHours(0, 0, 0, 0)

    const endTime = new Date(startTime.valueOf())
    endTime.setDate(endTime.getDate() + MAX_DAYS_TO_SYNC)

    console.log(`Query the next ${MAX_DAYS_TO_SYNC} days`)
    queryOption.timeMin = startTime.toISOString()
    queryOption.timeMax = endTime.toISOString()

    Object.assign(queryOption, options)

    return queryOption
  }

  /**
   * @return true if target event found
   */
  hasExistingTargetEvent(sourceEvent) {
    const targetId = this.getTargetId(sourceEvent)
    if (targetId) {
      if (Calendar.Events.get(this.targetCalendar.id, targetId)) return targetId
    }
    return false
  }

  /**
   * Return true if target event is legal
   */
  isLegalTargetEvent(targetEvent) {
    const sourceId = this.getSourceId(targetEvent)
    if (sourceId) {
      try {
        const sourceEvent = Calendar.Events.get(this.sourceCalendar.id, sourceId)
        return (this.getTargetId(sourceEvent) === targetEvent.id) && this.isEventSyncable(sourceEvent)
      } catch (e) {
        return false
      }
    }
    return false
  }

  /**
   * Return sourceId or false
   */
  getSourceId(event) {
    if (event.extendedProperties && event.extendedProperties.private) {
      return event.extendedProperties.private[this.sourceCalendar.id]
    }
    return false
  }

  /**
   * Return targetId or false
   */
  getTargetId(event) {
    if (event.extendedProperties && event.extendedProperties.private) {
      return event.extendedProperties.private[this.targetCalendar.id]
    }
    return false
  }

  /**
  * Returns true if event is a target event
  */
  isTargetEvent(event) {
    if (event.extendedProperties && event.extendedProperties.private) {
      return event.extendedProperties.private.sync === SYNC.TARGET
    }
    return false
  }

  /**
   * Creates an synced event
   */
  fromEventToTargetEvent(event) {
    const targetEvent = {
      summary: EVENT_SUMMARY,
      description: `Something is planned on calendar ${this.sourceCalendar.summary}`,
      extendedProperties: {
        private: {
          sync: SYNC.TARGET,
          [this.sourceCalendar.id]: event.id
        }
      },
      reminders: {
        useDefault: false,
        overrides: []
      },
      start: {},
      end: {}
    }
    if (event.start.dateTime) {
      targetEvent.start.dateTime = event.start.dateTime
      targetEvent.end.dateTime = event.end.dateTime
    } else {
      targetEvent.start.date = event.start.date
      targetEvent.end.date = event.end.date
    }
    return targetEvent
  }

  /**
   * Returns true if this event should be synced
   */
  isEventSyncable(event) {
    // Filter out "transparent" event
    if (event.transparency && event.transparency === 'transparent') return false

    if (event.status !== 'confirmed') return false

    // Filter out event created by sync
    if (this.isTargetEvent(event)) return false;

    return true
  }

  /**
   * Creates or updates events in target calendar
   */
  createOrUpdateTargetEvents(pageToken = null) {
    console.log('Creates & Updates events')
    let createOrUpdateRequests = []

    const sourceEventList = Calendar.Events.list(
      this.sourceCalendar.id,
      this.getTimedEventOptions({ pageToken })
    )
    sourceEventList.items.forEach(event => {
      if (!this.isEventSyncable(event)) return

      const request = {
        requestBody: this.fromEventToTargetEvent(event)
      }

      const targetId = this.hasExistingTargetEvent(event)
      if (targetId) {
        request.method = 'PATCH'
        request.endpoint = `https://www.googleapis.com/calendar/v3/calendars/${this.targetCalendar.id}/events/${targetId}`
        this.report.patched.total++
      } else {
        request.method = 'POST'
        request.endpoint = `https://www.googleapis.com/calendar/v3/calendars/${this.targetCalendar.id}/events`
        this.report.created.total++
      }

      createOrUpdateRequests.push(request)
    })

    if (sourceEventList.nextPageToken) {
      createOrUpdateRequests.push(...this.createOrUpdateTargetEvents(sourceEventList.nextPageToken))
    }

    return createOrUpdateRequests
  }

  /**
   * Removes all unrelevant events from target calendar
   */
  deleteUnrelevantTargetEvents(pageToken = null) {
    console.log('Deletes unrelevant events')
    let deleteRequests = []

    const targetEventList = Calendar.Events.list(
      this.targetCalendar.id,
      this.getTimedEventOptions({ pageToken, privateExtendedProperty: "sync=" + SYNC.TARGET })
    )

    targetEventList.items.forEach(event => {
      if (!this.isLegalTargetEvent(event)) {
        deleteRequests.push({
          method: 'DELETE',
          endpoint: `https://www.googleapis.com/calendar/v3/calendars/${this.targetCalendar.id}/events/${event.id}`,
        })
        this.report.deleted.total++
      }
    })

    if (targetEventList.nextPageToken) {
      deleteRequests.push(...this.deleteUnrelevantTargetEvents(targetEventList.nextPageToken))
    }

    return deleteRequests
  }

  /**
   * Sync events from source calendar to target calendar
   */
  syncAdd() {
    const createOrUpdateRequests = this.createOrUpdateTargetEvents()

    const updateSourceEventsRequests = []
    this.executeRequests(createOrUpdateRequests, (result) => {
      if (result.error) {
        console.error('Error create/update', result.error)
        this.report.patched.failed++
      } else {
        const sourceId = this.getSourceId(result)
        updateSourceEventsRequests.push({
          method: 'PATCH',
          endpoint: `https://www.googleapis.com/calendar/v3/calendars/${this.sourceCalendar.id}/events/${sourceId}`,
          requestBody: {
            extendedProperties: {
              private: {
                sync: SYNC.SOURCE,
                [this.targetCalendar.id]: result.id
              }
            }
          }
        })
      }
    })
    console.log('Update source events')
    this.executeRequests(updateSourceEventsRequests)

    console.log('SyncAdd done.', this.report);
  }

  /**
     * Sync events from source calendar to target calendar
     */
  syncRemove() {
    const deleteRequests = this.deleteUnrelevantTargetEvents()
    this.executeRequests(deleteRequests, result => {
      if (result.error) {
        this.report.deleted.failed++
        console.error('Error create/update', result)
      }
    })
    console.log('SyncRemove done.', this.report);
  }

  /**
   * Execute given requests in batch mode
   */
  executeRequests(allRequests, callback) {
    if (!allRequests || !allRequests.length) {
      console.log('No requests to process')
      return
    }
    let waitBetweenCall = WAIT_BETWEEN_BATCH_CALL
    do {
      const requests = allRequests.splice(0, MAX_REQUEST_PER_BATCH)
      const now = (new Date()).getTime()
      const delta = now - LATEST_BATCH_TIME
      if (delta < waitBetweenCall) {
        // Delay next called to prevent API limit rate triggering
        console.log(`Delaying next batch call for ${waitBetweenCall - delta} ms`)
        Utilities.sleep(waitBetweenCall - delta)
      }
      LATEST_BATCH_TIME = now
      console.log(`Processing ${requests.length} requests (remaining ${allRequests.length}).`);
      let results = BatchRequest.EDo({
        useFetchAll: true,
        batchPath: 'batch/calendar/v3',
        requests
      });

      let replayCount = 0
      for (let idx = results.length - 1; idx >= 0; idx--) {
        const result = results[idx]
        if (result.error) {
          if (result.error.code === 403) {
            if (result.error.message === 'Rate Limit Exceeded') {
              allRequests.push(requests[idx])
              results.splice(idx, 1)
              replayCount++
            }else if (result.error.message.startWith('Quota exceeded for quota metric \'Queries\' and limit \'Queries per minute per user')){
              console.error('Reached max quota, EXITING SCRIPT', result)
              exit(1)
            }
          }
        }
      }

      if (replayCount) {
        console.log(`RATE LIMITE REACHED, ${results.length} requests processed, ${replayCount} to replay.`);
        waitBetweenCall = waitBetweenCall * 2
      } else {
        waitBetweenCall = WAIT_BETWEEN_BATCH_CALL
        console.log(`${requests.length} requests processed.`);
      }

      if (callback) {
        results.forEach(callback)
      } else {
        results.forEach((result) => {
          if (result.error) {
            console.error('Execute query error', result)
          }
        })
      }
    } while (allRequests.length)
  }
}

function main() {
  //console.log('Your calendars', Calendar.CalendarList.list().items.map(({ id, summary }) => ({ id, summary })))
  const lock = LockService.getScriptLock();
  const properties = PropertiesService.getUserProperties();
  const RUN_PROPERTY = 'CalendarSyncRunAnother'

  const success = lock.tryLock(2000);
  if (!success) {
    console.log('Already running, register a run after the current one.');
    properties.setProperty(RUN_PROPERTY, true)
  } else {
    console.log('Lock acquiered, preventing concurrent run.');
    const p2wSyncer = new SyncCalendar(PERSONNAL_CALENDAR, WORK_CALENDAR)
    p2wSyncer.syncAdd()
    const w2pSyncer = new SyncCalendar(WORK_CALENDAR, PERSONNAL_CALENDAR)
    w2pSyncer.syncAdd()

    p2wSyncer.syncRemove()
    w2pSyncer.syncRemove()

    lock.releaseLock();

    if (properties.getProperty(RUN_PROPERTY)) {
      console.log('One more run has been requested during execution')
      properties.deleteProperty(RUN_PROPERTY)
      main()
    }
  }
}